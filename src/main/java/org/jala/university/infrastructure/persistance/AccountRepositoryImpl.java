package org.jala.university.infrastructure.persistance;

import jakarta.persistence.EntityManager;
import org.jala.university.commons.infrastructure.persistance.CrudRepository;
import org.jala.university.domain.entity.Account;
import org.jala.university.domain.repository.AccountRepository;

import java.util.UUID;

public class AccountRepositoryImpl extends CrudRepository<Account, UUID> implements AccountRepository {
    public AccountRepositoryImpl(EntityManager entityManager) {
        super(Account.class, entityManager);
    }
}
