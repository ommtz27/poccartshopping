package org.jala.university.domain.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.util.Date;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jala.university.commons.domain.BaseEntity;
import org.springframework.data.annotation.CreatedDate;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "creditCard")
public class CreditCard implements BaseEntity<UUID> {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    UUID id;

   @Column
    UUID userId;

    @Column
    String cardNumber;

    @Column
    Double creditLimit;

    @Column
    Double debt;

    @Column
    String name;

    @Column
    String ci;

    @Enumerated
    Tracking tracking;

    @Column
    Date expiryDate;

    @Column
    String cvv;

    @CreatedDate
    Date created;

    @Override
    public UUID getId() {
        return id;
    }
}
