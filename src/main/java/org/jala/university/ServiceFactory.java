package org.jala.university;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import org.jala.university.application.mapper.AccountMapper;
import org.jala.university.application.mapper.CreditCardMapper;
import org.jala.university.application.service.AccountService;
import org.jala.university.application.service.AccountServiceImpl;
import org.jala.university.application.service.CreditCardService;
import org.jala.university.application.service.CreditCardServiceImpl;
import org.jala.university.domain.repository.AccountRepository;
import org.jala.university.domain.repository.CreditCardRepo;
import org.jala.university.infrastructure.persistance.AccountRepositoryImpl;
import org.jala.university.infrastructure.persistance.CreditCardRepoImpl;

public class ServiceFactory {

    private static AccountService service;


    public static AccountService accountService() {
        if (service != null) {
            return service;
        }
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        AccountRepository accountRepository = new AccountRepositoryImpl(entityManager);
        AccountMapper accountMapper = new AccountMapper();
        service = new AccountServiceImpl(accountRepository, accountMapper);
        return service;
    }
}