package org.jala.university.presentation;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import org.jala.university.application.mapper.CreditCardMapper;
import org.jala.university.application.service.CreditCardService;
import org.jala.university.application.service.CreditCardServiceImpl;
import org.jala.university.domain.repository.CreditCardRepo;
import org.jala.university.infrastructure.persistance.CreditCardRepoImpl;

public class CreditCardServiceFactory {
    private static CreditCardService creditCardService;

    private CreditCardServiceFactory() {
    }

    public static CreditCardService CreditCardService() {
        if (creditCardService != null) {
            return creditCardService;
        }

        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        CreditCardRepo creditCardRepo = new CreditCardRepoImpl(entityManager);
        CreditCardMapper creditCardMapper = new CreditCardMapper();
        creditCardService = new CreditCardServiceImpl(creditCardRepo, creditCardMapper);
        return creditCardService;
    }
}
