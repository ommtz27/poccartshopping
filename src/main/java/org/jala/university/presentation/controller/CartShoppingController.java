package org.jala.university.presentation.controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import org.jala.university.commons.presentation.BaseController;

public class CartShoppingController extends BaseController implements Initializable {

  @FXML
  private FlowPane cartFlowPane;

  @FXML
  private FlowPane productsFlowPane;


  @Override
  public void initialize(URL url, ResourceBundle resourceBundle) {

    for (int i = 0; i < 10; i++) {
      productsFlowPane.getChildren().add(createProductPane("Producto "+i));
    }
  }

  @FXML
  void cancelPayment(ActionEvent event) {
    clearCart();
  }

  @FXML
  void confirmPayment(ActionEvent event) {
    Alert confirmation = new Alert(Alert.AlertType.CONFIRMATION);
    confirmation.setTitle("Confirmación de compra");
    confirmation.setHeaderText("¿Estás seguro de comprar estos productos?");
    confirmation.setContentText(getCartDetails());
    confirmation.showAndWait().ifPresent(response ->{
    if(response== ButtonType.OK){
      System.out.println("Compra realizada");
      clearCart();

    }});
  }

  private Pane createCartPane(String productName, Button addButton, int price) {
    Pane productPane = new Pane();
    productPane.setPrefSize(252, 80);
    productPane.setStyle("-fx-background-color: linear-gradient(to right, #e0eafc, #cfdef3); "
        + "-fx-background-radius: 10px; -fx-border-color: black; -fx-border-radius: 10px;");

    Label productNameLabel = new Label(productName);
    productNameLabel.setLayoutX(14.0);
    productNameLabel.setLayoutY(10.0);
    productNameLabel.setPrefSize(110.0, 70.0);
    productNameLabel.setFont(new Font(14.0));
    productNameLabel.setWrapText(true);
    productNameLabel.setAlignment(Pos.CENTER);


    Label priceLabel = new Label("$"+price+".00");
    priceLabel.setLayoutX(14.0);
    priceLabel.setLayoutY(20.0);
    priceLabel.setPrefSize(110.0, 15.0);
    priceLabel.setFont(new Font(12.0));
    priceLabel.setAlignment(Pos.CENTER);

    Button deleteButton = new Button("DELETE");
    deleteButton.setLayoutX(166.0);
    deleteButton.setLayoutY(46.0);
    deleteButton.setFont(new Font(10.0));
    deleteButton.setOnAction(event -> deleteProductPane(productPane, addButton));

    Spinner<Integer> productSpinner = new Spinner<>();
    productSpinner.setLayoutX(138.0);
    productSpinner.setLayoutY(14.0);
    productSpinner.setPrefSize(101.0, 24.0);
    productSpinner.setEditable(false);

    SpinnerValueFactory.IntegerSpinnerValueFactory valueFactory =
        new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 20, 1);
    productSpinner.setValueFactory(valueFactory);

    productSpinner.valueProperty().addListener((observable, oldValue, newValue) -> {
      updatePriceLabel(priceLabel, newValue,price);
    });

    productPane.getChildren().addAll(productNameLabel, priceLabel, deleteButton, productSpinner);
    return productPane;
  }

  private void updatePriceLabel(Label priceLabel, int quantity, int actualPrice) {
    double totalPrice = actualPrice * quantity;
    priceLabel.setText(String.format("$%.2f", totalPrice));
  }

  private Pane createProductPane(String productName) {
    int price = setRandomPrice();
    Pane mainPane = new Pane();
    mainPane.setPrefSize(200, 216);
    mainPane.setStyle("-fx-background-color: linear-gradient(to right, #dce35b, #45b649); "
        + "-fx-background-radius: 20px;");

    Pane innerPane = new Pane();
    innerPane.setLayoutX(16.0);
    innerPane.setLayoutY(22.0);
    innerPane.setPrefSize(167.0, 116.0);
    innerPane.setStyle("-fx-background-color: WHITE; -fx-background-radius: 5px;");

    Label productLabel = new Label(productName);
    productLabel.setAlignment(javafx.geometry.Pos.CENTER);
    productLabel.setContentDisplay(javafx.scene.control.ContentDisplay.CENTER);
    productLabel.setLayoutX(6.0);
    productLabel.setLayoutY(145.0);
    productLabel.setPrefSize(188.0, 24.0);
    productLabel.setFont(new Font("System Bold", 19.0));
    productLabel.setWrapText(true);
    productLabel.setTextAlignment(javafx.scene.text.TextAlignment.CENTER);

    Button addButton = new Button("AGREGAR");
    addButton.setLayoutX(100.0);
    addButton.setLayoutY(178.0);
    addButton.setMnemonicParsing(false);
    addButton.setOnAction(event -> {
      cartFlowPane.getChildren().add(createCartPane(productName, addButton,price));
      addButton.setDisable(true);
    });

    Label priceLabel = new Label("$"+price+".00");
    priceLabel.setLayoutX(25.0);
    priceLabel.setLayoutY(182.0);

    mainPane.getChildren().addAll(innerPane, productLabel, addButton, priceLabel);
    return mainPane;
  }

  private int setRandomPrice() {
    Random random = new Random();
    int randomPrice = 5 + random.nextInt(96);
    return randomPrice;
  }

  private void deleteProductPane(Pane productPane, Button addButton) {
    cartFlowPane.getChildren().remove(productPane);
    addButton.setDisable(false);
  }

  public void clearCart() {
    List<Node> productsInCart = new ArrayList<>(cartFlowPane.getChildren());
    for (Node node : productsInCart) {
      if (node instanceof Pane) {
        Pane productPane = (Pane) node;
        Button addButton = null;
        for (Node productNode : productsFlowPane.getChildren()) {
          if (productNode instanceof Pane) {
            Pane product = (Pane) productNode;
            for (Node childNode : product.getChildren()) {
              if (childNode instanceof Button && "AGREGAR".equals(((Button) childNode).getText())) {
                addButton = (Button) childNode;
                break;
              }
            }
          }
          if (addButton != null) {
            deleteProductPane(productPane, addButton);
          }
        }

      }
    }
  }


  public String getCartDetails() {
    StringBuilder details = new StringBuilder();
    double total = 0;

    for (Node node : cartFlowPane.getChildren()) {
      if (node instanceof Pane) {
        Pane productPane = (Pane) node;
        String productName = "";
        double productPrice = 0;

        for (Node childNode : productPane.getChildren()) {
          if (childNode instanceof Label) {
            Label label = (Label) childNode;
            if (label.getText().startsWith("$")) {
              productPrice = Double.parseDouble(label.getText().substring(1));
            } else {
              productName = label.getText();
            }
          }
        }

        details.append(productName).append(": $").append(productPrice).append("\n");
        total += productPrice;
      }
    }

    details.append("Total: $").append(total);
    return details.toString();
  }

}
