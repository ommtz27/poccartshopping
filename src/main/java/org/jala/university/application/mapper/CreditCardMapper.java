package org.jala.university.application.mapper;

import org.jala.university.application.dto.CreditCardDto;
import org.jala.university.commons.application.mapper.Mapper;
import org.jala.university.domain.entity.CreditCard;

public class CreditCardMapper implements Mapper<CreditCard, CreditCardDto> {


    @Override
    public CreditCardDto mapTo(CreditCard creditCard) {
        return CreditCardDto.builder()
                .id(creditCard.getId())
                .userId(creditCard.getUserId())
                .cardNumber(creditCard.getCardNumber())
                .creditLimit(creditCard.getCreditLimit())
                .tracking(creditCard.getTracking())
                .expiryDate(creditCard.getExpiryDate())
                .cvv(creditCard.getCvv())
                .debt(creditCard.getDebt())
                .name(creditCard.getName())
                .ci(creditCard.getCi())
                .build();
    }

    @Override
    public CreditCard mapFrom(CreditCardDto creditCardDto) {

        return CreditCard.builder()
                .id(creditCardDto.getId())
                .userId(creditCardDto.getUserId())
                .cardNumber(creditCardDto.getCardNumber())
                .creditLimit(creditCardDto.getCreditLimit())
                .tracking(creditCardDto.getTracking())
                .expiryDate(creditCardDto.getExpiryDate())
                .cvv(creditCardDto.getCvv())
                .debt(creditCardDto.getDebt())
                .name(creditCardDto.getName())
                .ci(creditCardDto.getCi())
                .build();
    }
}
