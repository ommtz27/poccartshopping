package org.jala.university.presentation.controller;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.jala.university.application.dto.CreditCardDto;
import org.jala.university.application.service.CreditCardService;
import org.jala.university.infrastructure.persistance.CreditCardUpdateMock;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class UpdateCreditCardTest {

  private CreditCardService service;
  private CreditCardUpdateMock mock;

  @BeforeEach
  void setUp(){
    service = Mockito.mock(CreditCardService.class);
    mock = new CreditCardUpdateMock(service);
  }

  @Test
  void update_ShouldBe_Succesful_WhenEveryThingIsFine(){
    CreditCardDto creditCardDto = mock.creditCardDtoData();
    when(service.createCreditCard(creditCardDto)).thenReturn(creditCardDto);

    mock.onUpdate();

    verify(service).updateCreditCard(creditCardDto);

  }

}
