package org.jala.university.infrastructure.persistance;

import java.time.YearMonth;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.UUID;
import org.jala.university.ServiceFactory;
import org.jala.university.application.dto.CreditCardDto;
import org.jala.university.application.service.CreditCardService;
import org.jala.university.domain.entity.CreditCard;
import org.jala.university.domain.entity.Tracking;
import org.jala.university.presentation.CreditCardServiceFactory;

public class CreditCardUpdateMock implements CreditCardService {

  final CreditCardService creditCardService;
  private CreditCardDto creditCard;

  public CreditCardUpdateMock(CreditCardService creditCardService) {
    this.creditCardService = creditCardService;
  }

  public CreditCardDto creditCardDtoData(){
    CreditCardDto creditCard = CreditCardDto.builder()
        .cardNumber(generateCardNumber())
        .userId(UUID.randomUUID())
        .name("Omar Martínez")
        .ci("ci")
        .creditLimit(5000.0)
        .cvv(generateCVV())
        .creditLimit(2000.0)
        .expiryDate(generateExpiryDate())
        .debt(0.0)
        .tracking(Tracking.ORDERED)
        .build();
    CreditCardService createCreditCard  = (CreditCardService) creditCardService.createCreditCard(creditCard);
    CreditCardService updateCreditCard  = (CreditCardService) creditCardService.createCreditCard(creditCard);
    return creditCard;
  }

  public void onUpdate(){
    CreditCardDto creditCard = creditCardDtoData();
    creditCard.setName("Mi nuevo nombre");
    creditCardService.updateCreditCard(creditCard);
  }

  public static String generateCardNumber() {
    StringBuilder cardNumber = new StringBuilder("5518");
    Random random = new Random();
    for (int i = 0; i < 12; i++) {
      int digit = random.nextInt(10);
      cardNumber.append(digit);
    }
    String cardNumberString = cardNumber.toString();
    return cardNumberString;
  }

  public static String generateCVV() {
    Random random = new Random();
    int cvv = 100 + random.nextInt(900);
    return String.valueOf(cvv);
  }

  public static Date generateExpiryDate() {
    Random random = new Random();
    int year = YearMonth.now().getYear() + 3; // Añade 3 años a partir del año actual
    int month = random.nextInt(12) + 1; // Mes aleatorio entre 1 y 12
    Calendar calendar = new GregorianCalendar();
    calendar.set(Calendar.YEAR, year);
    calendar.set(Calendar.MONTH, month - 1); // Meses en Calendar van de 0 a 11

    return calendar.getTime(); // Devolver la fecha como un objeto Date
  }

  @Override
  public CreditCardDto createCreditCard(CreditCardDto creditCardDto) {
    return null;
  }

  @Override
  public CreditCardDto getCreditCardbyId(UUID id) {
    return null;
  }

  @Override
  public void updateCreditCard(CreditCardDto creditCardDto) {

  }
}
