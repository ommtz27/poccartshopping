package org.jala.university.domain.repository;

import java.util.UUID;
import org.jala.university.commons.domain.Repository;
import org.jala.university.domain.entity.CreditCard;

public interface CreditCardRepo extends Repository<CreditCard, UUID> {}



