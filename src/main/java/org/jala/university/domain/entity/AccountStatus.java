package org.jala.university.domain.entity;

public enum AccountStatus {
    ACTIVE,
    CLOSED,
    FROZEN
}
