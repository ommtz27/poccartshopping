package org.jala.university.domain.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.jala.university.commons.domain.BaseEntity;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import java.util.Date;
import java.util.UUID;

@Entity
@Data
@Builder
@AllArgsConstructor
@Table(name = "accounts")
public class Account implements BaseEntity<UUID> {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    UUID id;

    @Column
    String accountNumber;

    @Column
    String name;

    @Column
    Double balance;

    @Enumerated(EnumType.STRING)
    AccountStatus status;

    @Enumerated(EnumType.STRING)
    Currency currency;

    @CreatedDate
    Date created;

    @LastModifiedDate
    Date updated;

    @Override
    public UUID getId() {
        return id;
    }

    public Account() {
        super();
    }
}
