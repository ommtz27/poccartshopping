package org.jala.university.presentation.controller;

import org.junit.jupiter.api.Test;

public class ValidationTest {

  public void validateOnlyNumbers(String entry) {
    if (!entry.matches("\\d*")) {
      System.out.println("Entrada inválida, solo ingresa números");
    }
  }

  public void validateOnlyLetters(String entry) {
    if (!entry.matches("[a-zA-Z ]*")) {
      System.out.println("Entrada inválida, solo ingresa letras");
    }
  }


  @Test
  public void testValidateEntry_OnlyNumbers() {
    String entry = "123345";
    validateOnlyNumbers(entry);
  }

  @Test
  public void testValidateEntry_OnlyCharacters() {
    String entry = "abcdef";
    validateOnlyLetters(entry);
  }


}
