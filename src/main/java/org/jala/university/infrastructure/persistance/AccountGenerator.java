package org.jala.university.infrastructure.persistance;

import org.jala.university.domain.entity.Account;
import org.jala.university.domain.entity.AccountStatus;
import org.jala.university.domain.entity.Currency;

import java.util.*;

/**
 * User: Joaquin Arrazola
 * Date: 27/4/24 11:58 AM
 */
public class AccountGenerator {

    private AccountGenerator() {
    }

    public static Map<UUID, Account> generateRandomAccounts(int size) {
        Map<UUID, Account> accounts = new HashMap<>();
        for (int i = 0; i < size; i++) {
            Account account = buildRandomAccount();
            accounts.put(account.getId(), account);
        }
        return accounts;
    }

    private static Account buildRandomAccount() {
        List<Currency> currencies =
                Collections.unmodifiableList(Arrays.asList(Currency.values()));
        List<AccountStatus> statusList =
                Collections.unmodifiableList(Arrays.asList(AccountStatus.values()));
        return Account.builder()
                .id(UUID.randomUUID())
                .name("Cuenta " + radonName())
                .balance(Math.random()+1000)
                .accountNumber(radonAccountNumber())
                .currency(currencies.get(getRandom().nextInt(currencies.size())))
                .status(statusList.get(getRandom().nextInt(statusList.size())))
                .build();

    }

    private static String radonName() {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = getRandom().nextInt(6);

        return getRandom().ints(leftLimit, rightLimit + 1)
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

    private static String radonAccountNumber() {
        int leftLimit = 48; // letter '0'
        int rightLimit = 57; // letter '9'
        int targetStringLength = 12;


        return getRandom().ints(leftLimit, rightLimit + 1)
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

    private static Random getRandom() {
        return new Random();
    }
}
