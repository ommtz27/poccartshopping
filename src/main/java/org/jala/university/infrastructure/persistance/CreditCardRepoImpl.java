package org.jala.university.infrastructure.persistance;

import jakarta.persistence.EntityManager;
import java.util.UUID;
import org.jala.university.commons.infrastructure.persistance.CrudRepository;
import org.jala.university.domain.entity.CreditCard;
import org.jala.university.domain.repository.CreditCardRepo;

public class CreditCardRepoImpl extends CrudRepository<CreditCard, UUID> implements CreditCardRepo {
    public CreditCardRepoImpl(EntityManager entityManager) {
        super(CreditCard.class, entityManager);
    }
}
