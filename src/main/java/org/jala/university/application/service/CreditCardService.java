package org.jala.university.application.service;

import java.util.UUID;
import org.jala.university.application.dto.CreditCardDto;

public interface CreditCardService {
    CreditCardDto createCreditCard(CreditCardDto creditCardDto);
    CreditCardDto getCreditCardbyId(UUID id);
    void updateCreditCard(CreditCardDto creditCardDto);
}
