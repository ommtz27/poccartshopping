package org.jala.university.application.service;

import java.util.UUID;
import org.jala.university.application.dto.CreditCardDto;
import org.jala.university.application.mapper.CreditCardMapper;
import org.jala.university.domain.entity.CreditCard;
import org.jala.university.domain.repository.CreditCardRepo;

public class CreditCardServiceImpl implements CreditCardService {
    private final CreditCardRepo creditCardRepo;
    private final CreditCardMapper creditCardMapper;

    public CreditCardServiceImpl(CreditCardRepo creditCardRepo, CreditCardMapper creditCardMapper) {
        this.creditCardRepo = creditCardRepo;
        this.creditCardMapper = creditCardMapper;
    }

    @Override
    public CreditCardDto createCreditCard(CreditCardDto creditCardDto) {
        CreditCard creditCard = creditCardMapper.mapFrom(creditCardDto);
        CreditCard saved = creditCardRepo.save(creditCard);
        return creditCardMapper.mapTo(saved);
    }

    @Override
    public CreditCardDto getCreditCardbyId(UUID id) {
        CreditCard creditCard = creditCardRepo.findById(id);
        return creditCardMapper.mapTo(creditCard);
    }

    @Override
    public void updateCreditCard(CreditCardDto creditCardDto) {
        CreditCard creditCard = creditCardMapper.mapFrom(creditCardDto);
        creditCardRepo.save(creditCard);
    }
}
