package org.jala.university.domain.entity;

public enum Tracking {
    ORDERED,
    PACKED,
    SHIPPED,
    IN_TRANSIT,
    DELIVERIED
}
