package org.jala.university.application.service;

import org.jala.university.application.dto.AccountDto;
import org.jala.university.application.mapper.AccountMapper;
import org.jala.university.domain.entity.Account;
import org.jala.university.domain.repository.AccountRepository;

import java.util.List;

public class AccountServiceImpl implements AccountService {
    private final AccountRepository accountRepository;
    private final AccountMapper accountMapper;

    public AccountServiceImpl(AccountRepository accountRepository, AccountMapper accountMapper) {
        this.accountRepository = accountRepository;
        this.accountMapper = accountMapper;
    }

    @Override
    public AccountDto createAccount(AccountDto accountDto) {
        Account account = accountMapper.mapFrom(accountDto);
        Account saved = accountRepository.save(account);
        return accountMapper.mapTo(saved);
    }

    @Override
    public void updateAccount(AccountDto accountDto) {
        Account account = accountMapper.mapFrom(accountDto);
        accountRepository.save(account);
    }

    @Override
    public List<AccountDto> getAllAccounts() {
        return accountRepository.findAll().stream().map(accountMapper::mapTo).toList();
    }

    @Override
    public void removeAccount(AccountDto account) {
        accountRepository.delete(accountMapper.mapFrom(account));
    }
}
