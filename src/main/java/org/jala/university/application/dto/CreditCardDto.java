package org.jala.university.application.dto;

import java.util.Date;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.jala.university.domain.entity.Tracking;

@Builder
@Data
@AllArgsConstructor
public class CreditCardDto {

    UUID id;
    UUID userId;
    String cardNumber;
    Double creditLimit;
    Double debt;
    String ci;
    String name;
    Tracking tracking;
    Date expiryDate;
    String cvv;









}
